const COLOR = {
  Black: "black",
  Red: "red",
  Green: "green",
  Yellow: "yellow",
  Blue: "blue",
  Magenta: "magenta",
  Cyan: "cyan",
  White: "white",

  BrightBlack: "bright_black",
  BrightRed: "bright_red",
  BrightGreen: "bright_green",
  BrightYellow: "bright_yellow",
  BrightBlue: "bright_blue",
  BrightMagenta: "bright_magenta",
  BrightCyan: "bright_cyan",
  BrightWhite: "bright_white",
};

const ESCAPE_SEQUENCE = {
  Reset: "\x1b[0m",
  Bold: "\x1b[1m",
  Dim: "\x1b[2m",
  Underscore: "\x1b[4m",
  Blink: "\x1b[5m",
  Reverse: "\x1b[7m",
  Hidden: "\x1b[8m",

  FgBlack: "\x1b[30m",
  FgRed: "\x1b[31m",
  FgGreen: "\x1b[32m",
  FgYellow: "\x1b[33m",
  FgBlue: "\x1b[34m",
  FgMagenta: "\x1b[35m",
  FgCyan: "\x1b[36m",
  FgWhite: "\x1b[37m",

  BgBlack: "\x1b[40m",
  BgRed: "\x1b[41m",
  BgGreen: "\x1b[42m",
  BgYellow: "\x1b[43m",
  BgBlue: "\x1b[44m",
  BgMagenta: "\x1b[45m",
  BgCyan: "\x1b[46m",
  BgWhite: "\x1b[47m",
};

const FORMAT = {
  Bold: "bold",
  Dim: "dim",
  Underscore: "underscore",
  Blink: "blink",
  Reverse: "reverse",
  Hidden: "hidden",
};

const setFormat = (format) => {
  const givenFormat =
    typeof format === "string" ? format.toLowerCase() : format;
  switch (givenFormat) {
    case FORMAT.Bold:
      return ESCAPE_SEQUENCE.Bold;
    case FORMAT.Dim:
      return ESCAPE_SEQUENCE.Dim;
    case FORMAT.Underscore:
      return ESCAPE_SEQUENCE.Underscore;
    case FORMAT.Blink:
      return ESCAPE_SEQUENCE.Blink;
    case FORMAT.Reverse:
      return ESCAPE_SEQUENCE.Reverse;
    case FORMAT.Hidden:
      return ESCAPE_SEQUENCE.Hidden;
    default:
      return ESCAPE_SEQUENCE.Reset;
  }
};

/**
 * @function setColor
 */
const setColor = (color) => {
  const givenColor = typeof color === "string" ? color.toLowerCase() : color;
  switch (givenColor) {
    case COLOR.Black:
      return ESCAPE_SEQUENCE.BgBlack;
    case COLOR.Red:
      return ESCAPE_SEQUENCE.BgRed;
    case COLOR.Green:
      return ESCAPE_SEQUENCE.BgGreen;
    case COLOR.Yellow:
      return ESCAPE_SEQUENCE.BgYellow;
    case COLOR.Blue:
      return ESCAPE_SEQUENCE.BgBlue;
    case COLOR.Magenta:
      return ESCAPE_SEQUENCE.BgMagenta;
    case COLOR.Cyan:
      return ESCAPE_SEQUENCE.BgCyan;
    case COLOR.White:
      return ESCAPE_SEQUENCE.BgWhite;
    case COLOR.BrightBlack:
      return ESCAPE_SEQUENCE.FgBlack;
    case COLOR.BrightRed:
      return ESCAPE_SEQUENCE.FgRed;
    case COLOR.BrightGreen:
      return ESCAPE_SEQUENCE.FgGreen;
    case COLOR.BrightYellow:
      return ESCAPE_SEQUENCE.FgYellow;
    case COLOR.BrightBlue:
      return ESCAPE_SEQUENCE.FgBlue;
    case COLOR.BrightMagenta:
      return ESCAPE_SEQUENCE.FgMagenta;
    case COLOR.BrightCyan:
      return ESCAPE_SEQUENCE.FgCyan;
    case COLOR.BrightWhite:
      return ESCAPE_SEQUENCE.FgWhite;
    default:
      return ESCAPE_SEQUENCE.Reset;
  }
};

const log = (color) => (name, ...args) =>
  console.log(
    `${setFormat(FORMAT.Bold)}${setColor(color)}[${name}]${
      ESCAPE_SEQUENCE.Reset
    }`,
    ...args
  );

module.exports = {
  FORMAT,
  COLOR,
  log,
};
