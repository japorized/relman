import { Knex } from "knex";

enum Table {
  Contacts = "contacts",
  ContactPrimaryNames = "contact_primary_names",
  Names = "names",
  NameOrders = "name_orders",
  Relationships = "relationships",
  Roles = "roles",
  Occupations = "occupations",
  Events = "events",
  EventParticipations = "event_participations",
}

const ParticipationChoices = ["joined", "did_not_join", "did_not_reply"];

export async function up(knex: Knex) {
  console.log("=== BEGIN init ===");
  await knex
    .transaction(async (trx) => {
      console.log(`Creating table ${Table.Contacts}`);
      await trx.schema.createTable(Table.Contacts, (table) => {
        table.bigIncrements("id").primary();
        table.date("date_of_birth").nullable();
        table.binary("contact_image").nullable();
        table.timestamp("created_at").notNullable().defaultTo(knex.fn.now());
        table.timestamp("updated_at").notNullable().defaultTo(knex.fn.now());
        table.timestamp("last_contacted_at").nullable();
        table
          .boolean("last_contacted_kept_in_view")
          .notNullable()
          .defaultTo(false);
      });

      console.log(`Creating table ${Table.NameOrders}`);
      await trx.schema.createTable(Table.NameOrders, (table) => {
        table.bigIncrements("id").primary();
        table.string("type").notNullable();
      });
      console.log(`Inserting initial values into ${Table.NameOrders}`);
      await trx(Table.NameOrders).insert([
        {
          type: "irrelevant",
        },
        {
          type: "first_middle_last",
        },
        {
          type: "family_middle_given",
        },
      ]);

      console.log(`Creating table ${Table.Names}`);
      await trx.schema.createTable(Table.Names, (table) => {
        table.bigIncrements("id").primary();
        table
          .bigInteger("contact_id")
          .notNullable()
          .references(`${Table.Contacts}.id`)
          .onDelete("CASCADE");
        table.string("given_name").notNullable();
        table.string("surname").nullable();
        table.string("middle_name").nullable();
        table.string("title").nullable();
        table
          .bigInteger("name_order_id")
          .notNullable()
          .references(`${Table.NameOrders}.id`);
        table.unique(["contact_id", "given_name", "surname", "middle_name"]);
      });

      console.log(`Creating table ${Table.ContactPrimaryNames}`);
      await trx.schema.createTable(Table.ContactPrimaryNames, (table) => {
        table
          .bigInteger("contact_id")
          .primary()
          .notNullable()
          .references(`${Table.Contacts}.id`)
          .onDelete("CASCADE");
        table
          .bigInteger("primary_name_id")
          .notNullable()
          .references(`${Table.Names}.id`);
      });

      console.log(`Creating table ${Table.Roles}`);
      await trx.schema.createTable(Table.Roles, (table) => {
        table.bigIncrements("id").primary();
        table.string("name").unique().notNullable();
      });

      console.log(`Creating table ${Table.Relationships}`);
      await trx.schema.createTable(Table.Relationships, (table) => {
        table
          .bigInteger("contact1_id")
          .notNullable()
          .references(`${Table.Contacts}.id`)
          .onDelete("CASCADE");
        table
          .bigInteger("contact2_id")
          .notNullable()
          .references(`${Table.Contacts}.id`)
          .onDelete("CASCADE");
        table
          .bigInteger("contact1_role_id")
          .notNullable()
          .references(`${Table.Roles}.id`);
        table
          .bigInteger("contact2_role_id")
          .notNullable()
          .references(`${Table.Roles}.id`);
        table.unique([
          "contact1_id",
          "contact2_id",
          "contact1_role_id",
          "contact2_role_id",
        ]);
      });

      console.log(`Creating table ${Table.Occupations}`);
      await trx.schema.createTable(Table.Occupations, (table) => {
        table.bigIncrements("id").primary();
        table
          .bigInteger("contact_id")
          .notNullable()
          .references(`${Table.Contacts}.id`)
          .onDelete("CASCADE");
        table.string("title").notNullable();
        table.text("organization").nullable();
        table.text("description").nullable();
        table.text("notes").nullable();
        table.timestamp("begin_at").nullable();
        table.timestamp("ended_at").nullable();
      });

      console.log(`Creating table ${Table.Events}`);
      await trx.schema.createTable(Table.Events, (table) => {
        table.bigIncrements("id").primary();
        table.text("title").notNullable();
        table.text("description").nullable();
        table.timestamp("date").nullable();
      });

      console.log(`Creating table ${Table.EventParticipations}`);
      await trx.schema.createTable(Table.EventParticipations, (table) => {
        table
          .bigInteger("event_id")
          .notNullable()
          .references(`${Table.Events}.id`)
          .onDelete("CASCADE");
        table
          .bigInteger("participant_id")
          .notNullable()
          .references(`${Table.Contacts}.id`)
          .onDelete("CASCADE");
        table.enu("participation", ParticipationChoices).notNullable();
        table.unique(["event_id", "participant_id"]);
      });
    })
    .then(() => {
      console.log("Migration successful");
    })
    .catch((error) => {
      console.error(error);
      console.log("Failed to migrate for init");
      throw error;
    })
    .finally(() => {
      console.log("=== END init ===");
    });
}

export async function down(knex: Knex) {
  return knex.transaction(async (trx) => {
    console.log("=== BEGIN init ===");
    await trx.schema.dropTable(Table.EventParticipations);
    await trx.schema.dropTable(Table.Events);
    await trx.schema.dropTable(Table.Occupations);
    await trx.schema.dropTable(Table.Relationships);
    await trx.schema.dropTable(Table.Roles);
    await trx.schema.dropTable(Table.ContactPrimaryNames);
    await trx.schema.dropTable(Table.Names);
    await trx.schema.dropTable(Table.Contacts);
    await trx.schema.dropTable(Table.NameOrders);
    console.log("=== END init ===");
  });
}
