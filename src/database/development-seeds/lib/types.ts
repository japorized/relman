export type Contact = {
  id: string;
  dateOfBirth: Date;
  createdAt: Date;
  updatedAt: Date;
};

export type Name = {
  id: string;
  contactId: string;
  givenName: string;
  surname?: string;
  middleName?: string;
  title?: string;
  nameOrderId: string;
};

export type Role = {
  id: string;
  name: string;
};

export type NameOrder = {
  id: string;
  type: string;
};
