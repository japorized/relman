import { Knex } from "knex";
import { Contact, Name, NameOrder, Role } from "./lib/types";

enum Table {
  Contacts = "contacts",
  Names = "names",
  NameOrders = "name_orders",
  ContactPrimaryNames = "contact_primary_names",
  Relationships = "relationships",
  Roles = "roles",
}

export async function seed(knex: Knex) {
  console.log("-- Begin 0001-init");

  console.log("Checking if contacts table is already populated");
  const existingContacts = await knex(Table.Contacts);
  if (existingContacts.length) {
    console.log("Contacts already exists");
    console.log("Skipping this seed...");
    return;
  }

  await knex.transaction(async (trx) => {
    console.log("Filling in some static data");
    await trx(Table.Roles).insert([
      { name: "Elder Brother" },
      { name: "Younger Brother" },
    ]);

    console.log("Creating some users");
    await trx(Table.Contacts).insert([
      {
        dateOfBirth: "1995-02-10",
        lastContactedKeptInView: true,
      },
      {
        dateOfBirth: "1999-01-01",
        lastContactedKeptInView: false,
      },
      {
        dateOfBirth: "2002-02-02",
        lastContactedKeptInView: false,
      },
      {
        dateOfBirth: "1999-02-02",
        lastContactedKeptInView: false,
      },
      {
        dateOfBirth: "1999-02-02",
        lastContactedKeptInView: false,
      },
      {
        dateOfBirth: "1999-02-02",
        lastContactedKeptInView: false,
      },
      {
        dateOfBirth: "1999-02-02",
        lastContactedKeptInView: false,
      },
      {
        dateOfBirth: "1999-02-02",
        lastContactedKeptInView: false,
      },
      {
        dateOfBirth: "1999-02-02",
        lastContactedKeptInView: false,
      },
      {
        dateOfBirth: "1999-02-02",
        lastContactedKeptInView: false,
      },
      {
        dateOfBirth: "1999-02-02",
        lastContactedKeptInView: false,
      },
      {
        dateOfBirth: "1999-02-02",
        lastContactedKeptInView: false,
      },
      {
        dateOfBirth: "1999-02-02",
        lastContactedKeptInView: false,
      },
      {
        dateOfBirth: "1999-02-02",
        lastContactedKeptInView: false,
      },
    ]);
    const contacts = await trx<Contact>(Table.Contacts);

    const nameOrders = await trx<NameOrder>(Table.NameOrders);
    await trx(Table.Names).insert([
      {
        contactId: contacts[0].id,
        givenName: "Andrew",
        surname: "Williams",
        middleName: "Dave",
        title: "Mr",
        nameOrderId: nameOrders[1].id,
      },
      {
        contactId: contacts[0].id,
        givenName: "Andy",
        nameOrderId: nameOrders[0].id,
      },
      {
        contactId: contacts[1].id,
        givenName: "Bill",
        surname: "Williams",
        nameOrderId: nameOrders[1].id,
      },
      {
        contactId: contacts[2].id,
        givenName: "Chris",
        surname: "Williams",
        nameOrderId: nameOrders[1].id,
      },
      {
        contactId: contacts[3].id,
        givenName: "Hassan",
        surname: "Badawi",
        nameOrderId: nameOrders[1].id,
      },
      {
        contactId: contacts[4].id,
        givenName: "Abu",
        surname: "Badawi",
        nameOrderId: nameOrders[1].id,
      },
      {
        contactId: contacts[5].id,
        givenName: "Bahrul",
        surname: "Badawi",
        nameOrderId: nameOrders[1].id,
      },
      {
        contactId: contacts[6].id,
        givenName: "Doha",
        surname: "Badawi",
        nameOrderId: nameOrders[1].id,
      },
      {
        contactId: contacts[7].id,
        givenName: "Elisa",
        surname: "Badawi",
        nameOrderId: nameOrders[1].id,
      },
      {
        contactId: contacts[8].id,
        givenName: "Faizul",
        surname: "Badawi",
        nameOrderId: nameOrders[1].id,
      },
      {
        contactId: contacts[9].id,
        givenName: "Ganaan",
        surname: "Badawi",
        nameOrderId: nameOrders[1].id,
      },
      {
        contactId: contacts[10].id,
        givenName: "Hafiz",
        surname: "Badawi",
        nameOrderId: nameOrders[1].id,
      },
      {
        contactId: contacts[11].id,
        givenName: "Izzi",
        surname: "Badawi",
        nameOrderId: nameOrders[1].id,
      },
      {
        contactId: contacts[12].id,
        givenName: "Jaafar",
        surname: "Badawi",
        nameOrderId: nameOrders[1].id,
      },
      {
        contactId: contacts[13].id,
        givenName: "Kassim",
        surname: "Badawi",
        nameOrderId: nameOrders[1].id,
      },
    ]);
    const names = await trx<Name>(Table.Names);

    await trx(Table.ContactPrimaryNames).insert([
      {
        contactId: contacts[0].id,
        primaryNameId: names[0].id,
      },
      {
        contactId: contacts[1].id,
        primaryNameId: names[2].id,
      },
      {
        contactId: contacts[2].id,
        primaryNameId: names[3].id,
      },
      {
        contactId: contacts[3].id,
        primaryNameId: names[4].id,
      },
      {
        contactId: contacts[4].id,
        primaryNameId: names[5].id,
      },
      {
        contactId: contacts[5].id,
        primaryNameId: names[6].id,
      },
      {
        contactId: contacts[6].id,
        primaryNameId: names[7].id,
      },
      {
        contactId: contacts[7].id,
        primaryNameId: names[8].id,
      },
      {
        contactId: contacts[8].id,
        primaryNameId: names[9].id,
      },
      {
        contactId: contacts[9].id,
        primaryNameId: names[10].id,
      },
      {
        contactId: contacts[10].id,
        primaryNameId: names[11].id,
      },
      {
        contactId: contacts[11].id,
        primaryNameId: names[12].id,
      },
      {
        contactId: contacts[12].id,
        primaryNameId: names[13].id,
      },
      {
        contactId: contacts[13].id,
        primaryNameId: names[14].id,
      },
    ]);

    console.log("Seed relationship between users");
    const roles = await trx<Role>(Table.Roles);
    await trx(Table.Relationships).insert([
      {
        contact1Id: contacts[0].id,
        contact2Id: contacts[1].id,
        contact1RoleId: roles[0].id,
        contact2RoleId: roles[1].id,
      },
      {
        contact1Id: contacts[0].id,
        contact2Id: contacts[2].id,
        contact1RoleId: roles[0].id,
        contact2RoleId: roles[1].id,
      },
      {
        contact1Id: contacts[1].id,
        contact2Id: contacts[2].id,
        contact1RoleId: roles[0].id,
        contact2RoleId: roles[1].id,
      },
    ]);
  });

  console.log("-- End 0001-init");
}
