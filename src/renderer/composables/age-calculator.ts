import { $dayjs } from "@renderer/plugins/dayjs";

export function setupAgeCalculator() {
  const calculateAge = (birthday: Date): number => {
    const today = $dayjs();
    const parsedBirthday = $dayjs(birthday);
    const birthdayThisYear = parsedBirthday.year(today.year());

    return (
      today.year() -
      parsedBirthday.year() -
      (today.isBefore(birthdayThisYear) ? 1 : 0)
    );
  };

  return {
    calculateAge,
  };
}
