import { useStore } from "vuex";

export function setupConfigurationInteracts() {
  const store = useStore();

  const saveConfigs = () => {
    const { ipcRenderer } = window;

    const colorscheme = store.getters["appState/colorscheme"];
    ipcRenderer.invoke("configs:save", { colorscheme });
  };

  return {
    saveConfigs,
  };
}
