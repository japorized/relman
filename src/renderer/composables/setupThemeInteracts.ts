import { computed } from "vue";
import { useStore } from "vuex";
import { BuiltInColorscheme } from "@renderer/utils/configs.type";

export function setupThemeInteracts() {
  const store = useStore();

  const colorscheme = computed(() => store.getters["appState/colorscheme"]);
  const setColorschemeToLight = () =>
    store.dispatch("appState/changeColorscheme", BuiltInColorscheme.Light);
  const setColorschemeToDark = () =>
    store.dispatch("appState/changeColorscheme", BuiltInColorscheme.Dark);
  const setColorschemeToSystem = () =>
    store.dispatch("appState/changeColorscheme", BuiltInColorscheme.System);

  return {
    colorscheme,
    setColorschemeToSystem,
    setColorschemeToDark,
    setColorschemeToLight,
  };
}
