import { Name, NameOrder, NameOrderType } from "@renderer/models/name.model";
import { useStore } from "vuex";

export function setupNameOrdering() {
  const store = useStore();
  const nameOrders: NameOrder[] = store.getters["nameOrders/nameOrders"];

  const orderName = (name: Omit<Name, "id" | "contactId">) => {
    // If the person has no surname (for any reason), just return their given name
    if (!name.surname) {
      return name.givenName;
    }

    const nameOrder = nameOrders.find(
      (nameOrder) => nameOrder.id === name.nameOrderId
    );
    switch (nameOrder?.type) {
      case NameOrderType.FamilyMiddleGiven:
        return `${name.surname}${
          name.middleName ? ` ${name.middleName}` : ""
        } ${name.givenName}`;
      case NameOrderType.FirstMiddleLast:
      case NameOrderType.Irrelevant:
      default:
        return `${name.givenName}${
          name.middleName ? ` ${name.middleName}` : ""
        }${name.surname ? ` ${name.surname}` : ""}`;
    }
  };

  return {
    nameOrders,
    orderName,
  };
}
