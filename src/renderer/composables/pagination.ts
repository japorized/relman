import { ref, Ref, computed } from "vue";

/**
 * Paginate a full collection of items.
 *
 * This is meant to be used with data that gets returned in full and not
 * paginated.
 *
 * @param items      Reference to an array of items to be paginated
 * @param pageSize   Size of each page
 *
 * @returns
 * - `currentPage` — Reference to the current page index (0-based)
 * - `totalPages` — Computed reference to the total number of pages
 * - `pagination` — Computed reference to a paginated result
 *
 * @example
 * The following is an example on how to use the returned values.
 * ```typescript
 * const contacts = ref([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
 * const { currentPage, totalPages, pagination } = paginateItems(contacts, 3);
 *
 * console.log(pagination); // returns [1, 2, 3]
 *
 * currentPage.value += 1;
 * console.log(pagination); // returns [4, 5, 6]
 * ```
 *
 * @remark
 * Note that it is sometimes entirely sensible for the main process to
 * return data in full, as there's little to no benefit in returning
 * paginated results. This is especially the case when the total amount of
 * rows to be obtained is almost always on the lesser side.
 */
export function paginateItems<T>(
  items: Ref<T[]>,
  pageSize: number | Ref<number>
) {
  const currentPage = ref(0);
  const size = computed(() =>
    typeof pageSize === "number" ? pageSize : pageSize.value
  );
  const totalPages = computed(() => Math.ceil(items.value.length / size.value));
  const pagination = computed(() => {
    const offset = currentPage.value * size.value;

    return items.value.slice(offset, offset + size.value);
  });

  return {
    currentPage,
    totalPages,
    pagination,
  };
}

/**
 * Prepare pagination utility references
 *
 * In contrast to {@link paginateItems}, this function simply returns the
 * common variables for pagination as references.
 *
 * @param [pageSize=50]   Page size to be set
 *
 * @returns
 * - `currentPage` — Reference to the current page index (0-based)
 * - `totalPages` — Reference to the total number of pages
 * - `pageSize` — Reference to the current size of pages
 */
export function prepareForPagination(pageSize = 50) {
  return {
    currentPage: ref(0),
    totalPages: ref(0),
    pageSize: ref(pageSize),
  };
}
