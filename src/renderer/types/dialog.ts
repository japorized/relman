export enum DialogType {
  Info = "info",
  Confirm = "confirm",
  // Prompt = "prompt", // We'll come back to this later on, if we need it
}

export enum YesNoDialogDefaultChoice {
  No = "no",
  Yes = "yes",
}
