export enum MenuItemType {
  Choice = "choice",
}

export type MenuItem = {
  type: MenuItemType;
  text: string;
  // eslint-disable-next-line @typescript-eslint/ban-types
  action?: Function;
};

export enum MenuOrientation {
  TopLeft = "top_left",
  TopRight = "top_right",
  LeftTop = "left_top",
  LeftCenter = "left_center",
  LeftBottom = "left_bottom",
  RightTop = "right_top",
  RightCenter = "right_center",
  RightBottom = "right_bottom",
  BottomLeft = "bottom_left",
  BottomRight = "bottom_right",
}
