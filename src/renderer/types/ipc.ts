export type PagedResult<T> = {
  results: T[];
  total: number;
  totalPages: number;
  currentPage: number;
};
