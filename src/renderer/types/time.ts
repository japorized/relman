export enum TimeFormat {
  TwelveHourFormat = "12_hour_format",
  TwentyFourHourFormat = "24_hour_format",
}
