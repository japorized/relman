import {
  Name,
  NameOrder,
  NameOrderType,
  NewName,
  NameChangeSet,
  PrettyNameOrder,
  PrettyNameOrderType,
} from "@renderer/models/name.model";

export enum NamesServiceIpc {
  Create = "names:create",
  AddNamesForContactId = "names:add_names_for_contact_id",
  UpdateNameById = "names:update_name_by_id",
  RemoveNameById = "names:remove_name_by_id",
  RemoveNamesByIds = "names:remove_names_by_ids",
  GetNameOrders = "names:get_name_orders",
  SetPrimaryNameForContact = "names:set_primary_name_for_contact",
}

export const NamesService = {
  async create(newName: NewName): Promise<Name> {
    return window.ipcRenderer.invoke(NamesServiceIpc.Create, newName);
  },

  /**
   * Get prettified name orders for display
   */
  async getPrettyNameOrders(): Promise<PrettyNameOrder[]> {
    const nameOrders = await window.ipcRenderer.invoke(
      NamesServiceIpc.GetNameOrders
    );
    return nameOrders.map((nameOrder: NameOrder) => ({
      id: nameOrder.id,
      type: prettifyNameOrderString(nameOrder.type),
    }));
  },

  /**
   * Add names for a particular contact by their ID
   */
  async addNamesForContactId(
    contactId: number,
    newNames: NewName[]
  ): Promise<Name[]> {
    return window.ipcRenderer.invoke(
      NamesServiceIpc.AddNamesForContactId,
      contactId,
      newNames
    );
  },

  async updateNameById(
    id: number,
    nameChangeSet: NameChangeSet
  ): Promise<Name> {
    return window.ipcRenderer.invoke(
      NamesServiceIpc.UpdateNameById,
      id,
      nameChangeSet
    );
  },

  async removeNameById(id: number) {
    return window.ipcRenderer.invoke(NamesServiceIpc.RemoveNameById, id);
  },

  async removeNamesByIds(ids: number[]) {
    return window.ipcRenderer.invoke(
      NamesServiceIpc.RemoveNamesByIds,
      ids.join(",")
    );
  },

  /**
   * Set the primary name for a contact
   */
  async setPrimaryNameForContact(contactId: number, nameId: number) {
    return window.ipcRenderer.invoke(
      NamesServiceIpc.SetPrimaryNameForContact,
      contactId,
      nameId
    );
  },

  /**
   * Get name orders from the main process, from the database
   */
  async getNameOrders(): Promise<NameOrder[]> {
    return window.ipcRenderer.invoke(NamesServiceIpc.GetNameOrders);
  },
};

/**
 * Returns the pretty name order based on the given name order type
 * @param nameOrderType   Any of the members of the NameOrderType enum
 *
 * @return Prettified string of the name order type
 */
export function prettifyNameOrderString(
  nameOrderType: NameOrderType
): PrettyNameOrderType {
  switch (nameOrderType) {
    case NameOrderType.Irrelevant:
      return PrettyNameOrderType.Irrelevant;
    case NameOrderType.FamilyMiddleGiven:
      return PrettyNameOrderType.FamilyMiddleGiven;
    case NameOrderType.FirstMiddleLast:
      return PrettyNameOrderType.FirstMiddleLast;
  }
}
