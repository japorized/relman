import {
  Contact,
  ContactChangeSet,
  ContactRelationship,
  DbContact,
  NewContact,
} from "@renderer/models/contact.model";
import { PagedResult } from "../types/ipc";
import { uint8arrayToImageurl } from "../utils/uint8array-to-imageurl";

export enum ContactServiceIpc {
  Create = "contacts:create",
  GetAll = "contacts:get_all",
  GetOneById = "contacts:get_one_by_id",
  GetManyById = "contacts:get_many_by_ids",
  UpdateContactById = "contacts:update_contact_by_id",
  FindMatchesByGivenName = "contacts:find_matches_by_given_name",
  DeleteOneById = "contacts:delete_one_by_id",
  DeleteManyById = "contacts:delete_many_by_ids",
  SearchAll = "contacts:search_all",
  GetKeptInViews = "contacts:get_kept_in_views",
  GetNotKeptInViews = "contacts:get_not_kept_in_views",
  SearchNotKeptInViews = "contacts:search_not_kept_in_views",
  KeepInViewByIds = "contacts:keep_in_view_by_ids",
  RemoveFromViewByIds = "contacts:remove_from_view_by_ids",
  QueryContactsTable = "contacts:query_contacts_table",
  GetUpcomingBirthdays = "contacts:get_upcoming_birthdays",
}

export const ContactsService = {
  async create(newContact: NewContact) {
    return window.ipcRenderer.invoke(ContactServiceIpc.Create, newContact);
  },

  async getAll(page = 0, limit = 50): Promise<PagedResult<Contact>> {
    const paginatedResult: PagedResult<DbContact> =
      await window.ipcRenderer.invoke(ContactServiceIpc.GetAll, page, limit);
    return convertPagedDbContactResultsToPagedContactResults(paginatedResult);
  },

  async getOneById(id: string): Promise<Contact> {
    const dbContact = await window.ipcRenderer.invoke(
      ContactServiceIpc.GetOneById,
      id
    );
    return convertDbContactToContact(dbContact);
  },

  async updateContactById(id: string, contactChangeSet: ContactChangeSet) {
    return window.ipcRenderer.invoke(
      ContactServiceIpc.UpdateContactById,
      id,
      contactChangeSet
    );
  },

  async findMatchesByGivenName(
    givenName: string,
    page = 0,
    limit = 50
  ): Promise<PagedResult<Contact>> {
    const paginatedResult = await window.ipcRenderer.invoke(
      ContactServiceIpc.FindMatchesByGivenName,
      givenName,
      page,
      limit
    );
    return convertPagedDbContactResultsToPagedContactResults(paginatedResult);
  },

  async deleteOneById(id: string): Promise<void> {
    return window.ipcRenderer.invoke(ContactServiceIpc.DeleteOneById, id);
  },

  async deleteManyById(ids: string[]): Promise<void> {
    return window.ipcRenderer.invoke(ContactServiceIpc.DeleteManyById, ids);
  },

  async searchAll(
    searchTerm: string,
    page = 0,
    limit = 50
  ): Promise<PagedResult<Contact>> {
    const paginatedResult: PagedResult<DbContact> =
      await window.ipcRenderer.invoke(
        ContactServiceIpc.SearchAll,
        searchTerm,
        page,
        limit
      );

    return convertPagedDbContactResultsToPagedContactResults(paginatedResult);
  },

  async getKeptInViews(page = 0, limit = 50): Promise<PagedResult<Contact>> {
    const paginatedResult: PagedResult<DbContact> =
      await window.ipcRenderer.invoke(
        ContactServiceIpc.GetKeptInViews,
        page,
        limit
      );
    return convertPagedDbContactResultsToPagedContactResults(paginatedResult);
  },

  async getNotKeptInViews(page = 0, limit = 50): Promise<PagedResult<Contact>> {
    const paginatedResult: PagedResult<DbContact> =
      await window.ipcRenderer.invoke(
        ContactServiceIpc.GetNotKeptInViews,
        page,
        limit
      );
    return convertPagedDbContactResultsToPagedContactResults(paginatedResult);
  },

  async searchNotKeptInViews(searchTerm: string) {
    const results = await window.ipcRenderer.invoke(
      ContactServiceIpc.SearchNotKeptInViews,
      searchTerm
    );

    return results.map(convertDbContactToContact);
  },

  async keepInViewByIds(ids: string[]) {
    return window.ipcRenderer.invoke(
      ContactServiceIpc.KeepInViewByIds,
      ids.join(",")
    );
  },

  async removeFromViewByIds(ids: string[]) {
    return window.ipcRenderer.invoke(
      ContactServiceIpc.RemoveFromViewByIds,
      ids.join(",")
    );
  },

  async queryContactsTable(
    queryParams?: {
      relations?: string;
      where?: [string, unknown][];
      whereIn: [string, unknown[]][];
    },
    page = 0,
    limit = 50
  ) {
    const paginatedResult: PagedResult<DbContact> =
      await window.ipcRenderer.invoke(
        ContactServiceIpc.QueryContactsTable,
        queryParams,
        page,
        limit
      );

    return convertPagedDbContactResultsToPagedContactResults(paginatedResult);
  },

  async getUpcomingBirthdays(daysAway = 14) {
    const results = await window.ipcRenderer.invoke(
      ContactServiceIpc.GetUpcomingBirthdays,
      daysAway
    );

    return results.map(convertDbContactToContact);
  },
};

function convertDbContactToContact(dbContact: DbContact): Contact {
  let contactImage: string | undefined = undefined;
  let relationships: ContactRelationship[] = [];
  if (dbContact.contactImage)
    contactImage = uint8arrayToImageurl(dbContact.contactImage);
  if (dbContact.relationships) {
    relationships = dbContact.relationships.map((relationship) => {
      let relContactImage: string | undefined = undefined;
      if (relationship.contactImage)
        relContactImage = uint8arrayToImageurl(relationship.contactImage);

      return {
        ...relationship,
        relationships: undefined,
        contactImage: relContactImage,
      };
    });
  }

  return {
    ...dbContact,
    relationships,
    contactImage,
  };
}

function convertPagedDbContactResultsToPagedContactResults(
  pagedDbContactResults: PagedResult<DbContact>
): PagedResult<Contact> {
  return {
    ...pagedDbContactResults,
    results: pagedDbContactResults.results.map(convertDbContactToContact),
  };
}
