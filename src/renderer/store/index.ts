import { Configurations } from "@renderer/utils/configs.type";
import { createLogger, createStore } from "vuex";
import appState from "./modules/app-state";
import nameOrders from "./modules/name-orders";

export const configs: Configurations =
  window.ipcRenderer.sendSync("configs:get");

const debug = process.env.NODE_ENV !== "production";

export default createStore({
  modules: {
    appState: appState(configs),
    nameOrders,
  },
  strict: debug,
  plugins: debug ? [createLogger()] : [],
});
