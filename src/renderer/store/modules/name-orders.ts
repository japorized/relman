import {
  NamesServiceIpc,
  prettifyNameOrderString,
} from "@/renderer/services/names.service";
import { NameOrder, PrettyNameOrder } from "@renderer/models/name.model";
import { Commit } from "vuex";

export type State = {
  nameOrders: NameOrder[];
  prettyNameOrders: PrettyNameOrder[];
};

const state: State = {
  nameOrders: [],
  prettyNameOrders: [],
};

const actions = {
  async loadNameOrders({ commit, state }: { commit: Commit; state: State }) {
    if (state.nameOrders.length === 0) {
      const nameOrders = await window.ipcRenderer.invoke(
        NamesServiceIpc.GetNameOrders
      );
      commit("setNameOrders", nameOrders);
    }
  },
};

const mutations = {
  setNameOrders(state: State, payload: NameOrder[]) {
    state.nameOrders = payload;
    state.prettyNameOrders = payload.map((nameOrder) => ({
      id: nameOrder.id,
      type: prettifyNameOrderString(nameOrder.type),
    }));
  },
};

const getters = {
  nameOrders(state: State) {
    return state.nameOrders;
  },
  prettyNameOrders(state: State) {
    return state.prettyNameOrders;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
