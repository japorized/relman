import {
  BuiltInColorscheme,
  Motion,
  Configurations,
} from "@renderer/utils/configs.type";
import { Commit } from "vuex";

function initializeColorscheme(configs: Configurations) {
  if (configs.colorscheme) {
    document.body.classList.add(configs.colorscheme);
    return configs.colorscheme;
  }
  {
    return BuiltInColorscheme.System;
  }
}

function initializeMotionPreference(configs: Configurations) {
  if (configs.motion && configs.motion !== Motion.Default) {
    document.body.classList.add(configs.motion);
    return configs.motion;
  } else {
    return Motion.Default;
  }
}

export type State = {
  colorscheme: BuiltInColorscheme | string;
  motion: Motion | string;
};

const state = (configs: Configurations) => (): State => ({
  colorscheme: initializeColorscheme(configs),
  motion: initializeMotionPreference(configs),
});

const actions = {
  changeColorscheme({ commit }: { commit: Commit }, newColorscheme: string) {
    document.body.classList.remove(...Object.values(BuiltInColorscheme));
    if (newColorscheme !== BuiltInColorscheme.System) {
      document.body.classList.add(newColorscheme);
    }
    commit("setColorscheme", newColorscheme);
  },
  changeMotion({ commit }: { commit: Commit }, newMotionPreference: Motion) {
    document.body.classList.remove(...Object.values(Motion));
    if (newMotionPreference !== Motion.Default) {
      document.body.classList.add(newMotionPreference);
    }
    commit("setMotionPreference", newMotionPreference);
  },
};

const mutations = (configs: Configurations) => ({
  setColorscheme(state: State, newColorscheme: string) {
    configs.colorscheme = newColorscheme;
    state.colorscheme = newColorscheme;
  },
  setMotionPreference(state: State, newMotionPreference: Motion) {
    configs.motion = newMotionPreference;
    state.motion = newMotionPreference;
  },
});

const getters = {
  colorscheme(state: State) {
    return state.colorscheme;
  },
  motion(state: State) {
    return state.motion;
  },
};

export default (configs: Configurations) => ({
  namespaced: true,
  state: state(configs),
  actions,
  mutations: mutations(configs),
  getters,
});
