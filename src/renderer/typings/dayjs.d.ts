import dayjs from "dayjs/esm";

declare module "@vue/runtime-core" {
  export interface ComponentCustomProperties {
    $dayjs: typeof dayjs;
  }
}
