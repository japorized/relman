import { dialogOptions } from "@renderer/plugins/dialog";

declare module "@vue/runtime-core" {
  export interface ComponentCustomProperties {
    $dialog: typeof dialogOptions;
  }
}
