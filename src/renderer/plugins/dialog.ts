import Dialog from "@renderer/components/Dialog.vue";
import { App, createApp } from "vue";
import { DialogType } from "@renderer/types/dialog";
import { FocusTrap } from "focus-trap-vue";

async function makeDialog<T>(
  message: string,
  dialogType: DialogType,
  props?: Record<string, unknown>
): Promise<T> {
  const dialogApp = createApp(Dialog, {
    message,
    type: dialogType,
    ...props,
  });
  dialogApp.component("FocusTrap", FocusTrap);
  const dialog = dialogApp.mount(document.createElement("div"));

  const response = (await dialog.pop()) as T;
  dialogApp.unmount();

  return response;
}

export const dialogOptions = {
  info: async (
    message: string,
    props?: Record<string, unknown>
  ): Promise<void> => makeDialog<void>(message, DialogType.Info, props),
  confirm: async (
    message: string,
    props?: Record<string, unknown>
  ): Promise<boolean> =>
    makeDialog<boolean>(message, DialogType.Confirm, props),
};

export default {
  install: (app: App) => {
    app.config.globalProperties.$dialog = dialogOptions;
  },
};
