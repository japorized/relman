import dayjs from "dayjs/esm";
import durationPlugin from "dayjs/esm/plugin/duration";
import utcPlugin from "dayjs/esm/plugin/utc";
import timezonePlugin from "dayjs/esm/plugin/timezone";
import relativeTimePlugin from "dayjs/esm/plugin/relativeTime";
import { App } from "vue";

dayjs.extend(durationPlugin);
dayjs.extend(utcPlugin);
dayjs.extend(timezonePlugin);
dayjs.extend(relativeTimePlugin);

export const $dayjs = dayjs;

export default {
  install: (app: App) => {
    app.config.globalProperties.$dayjs = dayjs;
  },
};
