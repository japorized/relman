import { createApp } from "vue";
import App from "./App.vue";
import "./assets/css/base.css";
import "./assets/css/colors.css";
import "./assets/css/reset.css";
import { router } from "./router";
import store from "./store/index";
import dayjsPlugin from "./plugins/dayjs";
import dialogPlugin from "./plugins/dialog";
import { FocusTrap } from "focus-trap-vue";

// Fork-awesome
import "fork-awesome/css/fork-awesome.min.css";

// Import vue-toastification
import Toast, { POSITION } from "vue-toastification";
import "vue-toastification/dist/index.css";
const toastConfigurations = {
  position: POSITION.TOP_RIGHT,
  timeout: 5000,
  closeOnClick: true,
  draggable: true,
  draggablePercent: 0.6,
  pauseOnHover: true,
  pauseOnFocusLoss: true,
  showCloseButtonOnHover: true,
  hideProgressBar: true,
  icon: false,
  closeButton: "button",
};

const app = createApp(App);

app
  .use(router)
  .use(store)
  .use(Toast, toastConfigurations)
  .use(dayjsPlugin)
  .use(dialogPlugin);
app.component("FocusTrap", FocusTrap);

app.mount("#app");
