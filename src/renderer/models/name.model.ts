export enum NameOrderType {
  Irrelevant = "irrelevant",
  FirstMiddleLast = "first_middle_last",
  FamilyMiddleGiven = "family_middle_given",
}

export enum PrettyNameOrderType {
  Irrelevant = "Irrelevant",
  FirstMiddleLast = "First-Middle-Last",
  FamilyMiddleGiven = "Family-Middle-Given",
}

export type NameOrder = {
  id: number;
  type: NameOrderType;
};

export type PrettyNameOrder = Pick<NameOrder, "id"> & {
  type: PrettyNameOrderType;
};

export type Name = {
  id: number;
  contactId: number;
  givenName: string;
  surname?: string;
  middleName?: string;
  title?: string;
  nameOrderId: number;
};

export type NewName = Pick<
  Name,
  "contactId" | "givenName" | "surname" | "middleName" | "title" | "nameOrderId"
>;

export type NameChangeSet = Partial<NewName> & Partial<Pick<Name, "id">>;

export type NameInEdit = NewName & {
  id?: number;
  toBeRemoved: boolean;
  isPrimary: boolean;
};
