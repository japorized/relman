import { Name, NameInEdit, NewName } from "./name.model";

export type ContactRelationship = Contact & {
  contactRole: string;
  relationshipRole: string;
};

export type DbContactRelationship = DbContact & {
  contactRole: string;
  relationshipRole: string;
};

export type Contact = {
  id: number;
  dateOfBirth?: string;
  contactImage?: string;
  primaryName: Name;
  createdAt: Date;
  updatedAt: Date;
  lastContactedAt?: Date;
  lastContactedKeptInView: boolean;
  names?: Name[];
  relationships?: ContactRelationship[];
  daysFromThisYearsBirthday?: number;
  daysFromNextYearsBirthday?: number;
};

export type DbContact = Omit<Contact, "contactImage" | "relationships"> & {
  contactImage?: Uint8Array;
  relationships?: DbContactRelationship[];
};

export type NewContact = Pick<Contact, "dateOfBirth" | "lastContactedAt"> & {
  lastContactedKeptInView?: boolean;
  contactImage?: ArrayBuffer;
  primaryName: NewName;
};

export type ContactChangeSet = Partial<Omit<NewContact, "primaryName">>;

export type ContactInEdit = Omit<ContactChangeSet, "names"> & {
  names?: NameInEdit[];
};
