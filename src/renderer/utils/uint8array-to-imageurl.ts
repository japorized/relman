export function uint8arrayToImageurl(uint8array: Uint8Array): string {
  return URL.createObjectURL(new Blob([uint8array], { type: "image/png" }));
}
