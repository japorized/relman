/**
 * Make keys specified in U optional in T
 */
export type MakeOptional<T, U extends keyof T> = Partial<T> & Omit<T, U>;
