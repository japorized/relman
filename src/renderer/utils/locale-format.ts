import { $dayjs } from "@renderer/plugins/dayjs";

export const LocaleFormat = {
  MediumStyleDate: (rawDate: string): string =>
    Intl.DateTimeFormat(undefined, { dateStyle: "medium" }).format(
      $dayjs(rawDate).toDate()
    ),
  MediumStyleDateTime: (rawTimestamp: string): string =>
    Intl.DateTimeFormat(undefined, {
      dateStyle: "medium",
      timeStyle: "short",
    }).format($dayjs(rawTimestamp).toDate()),
};
