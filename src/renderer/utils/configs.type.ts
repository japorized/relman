export enum Motion {
  Default = "default",
  Reduced = "reduced",
}

export enum BuiltInColorscheme {
  Light = "light",
  Dark = "dark",
  System = "system",
}

export type FilePath = {
  rawDir: string;
  dir: string;
  rawFilename: string;
  filename: string;
  readonly rawFullPath: string;
  readonly fullpath: string;
};

export type Configurations = {
  colorscheme: BuiltInColorscheme | string;
  motion: Motion;
  databaseFile: FilePath;
};
