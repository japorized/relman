import { ipcRenderer } from "electron";
window.ipcRenderer = ipcRenderer;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function attachExternalLinkEventListener(event: any) {
  if (
    event.target.tagName.toLowerCase() === "a" &&
    event.target.attributes.href.value.startsWith("http")
  ) {
    event.preventDefault();
    ipcRenderer.invoke("os:open_external_link", event.target.href);
  }
}

document.addEventListener("click", attachExternalLinkEventListener);

window.addEventListener("unload", () => {
  document.removeEventListener("click", attachExternalLinkEventListener);
});
