import { resolve as resolvePath } from "path";
import { app } from "electron";

const appName = "relman";
const dataDir = (() => {
  switch (process.platform) {
    case "linux":
      return process.env["XDG_DATA_HOME"]
        ? resolvePath(process.env["XDG_DATA_HOME"], appName)
        : resolvePath(app.getPath("home"), ".data", appName);
    default:
      return resolvePath(app.getPath("appData"), appName, "user-data");
  }
})();
app.setName("relman");
app.setPath("cache", resolvePath(app.getPath("cache"), app.getName()));
app.setPath("userData", dataDir);
app.setPath("logs", resolvePath(app.getPath("cache"), "logs"));

export const $app = app;
