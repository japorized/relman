import { Model, Page } from "objection";

export type PagedResult<T extends Model> = Page<T> & {
  totalPages: number;
  currentPage: number;
};

export function formatPagedResult<T extends Model>(
  fetched: Page<T>,
  currentPage: number,
  limit: number
): PagedResult<T> {
  return {
    ...fetched,
    totalPages: Math.ceil(fetched.total / limit),
    currentPage,
  };
}
