export type Service = {
  // eslint-disable-next-line @typescript-eslint/ban-types
  [key: string]: Function;
};
