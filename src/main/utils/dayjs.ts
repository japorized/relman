import dayjs from "dayjs";
import durationPlugin from "dayjs/plugin/duration";
import utcPlugin from "dayjs/plugin/utc";
import timezonePlugin from "dayjs/plugin/timezone";
import relativeTimePlugin from "dayjs/plugin/relativeTime";

dayjs.extend(durationPlugin);
dayjs.extend(utcPlugin);
dayjs.extend(timezonePlugin);
dayjs.extend(relativeTimePlugin);

/**
 * A plugins-included dayjs function
 * Plugins included:
 * - duration
 * - utc
 * - timezone
 * - relativeTime
 */
export const $dayjs = dayjs;
