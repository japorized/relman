import { shell } from "electron";
import { Service } from "@main/utils/module-type-utilities";

export const OsService: Service = {
  openExternalLink: async (href: string) => {
    shell.openExternal(href);
  },
};
