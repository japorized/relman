import { Role } from "@main/models/role.model";
import { TransactionOrKnex } from "objection";

export const RolesService = {
  create,
  getRoles,
};

export function create(names: string[], trx?: TransactionOrKnex) {
  return Role.query(trx).insert(
    names.map((name) => ({
      name,
    }))
  );
}

export function getRoles(trx?: TransactionOrKnex) {
  return Role.query(trx);
}
