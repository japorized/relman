import {
  Name,
  NameChangeSet,
  NameOrder,
  NewName,
} from "@main/models/name.model";
import { Service } from "@main/utils/module-type-utilities";
import { TransactionOrKnex } from "objection";
import { NameDoesNotBelongToContact, NameNotFound } from "../lib/exceptions";
import { ContactPrimaryName } from "../models/contact-primary-name.model";

export const NamesService: Service = {
  create,
  addNamesForContactId,
  updateNameById,
  removeNameById,
  removeNamesByIds,
  setPrimaryNameForContact,
  getNameOrders,
};

export function create(name: NewName, trx?: TransactionOrKnex) {
  return Name.query(trx).insert({
    contactId: name.contactId,
    givenName: name.givenName,
    surname: name?.surname ?? undefined,
    middleName: name?.middleName ?? undefined,
    title: name?.title ?? undefined,
    nameOrderId: name.nameOrderId,
  });
}

export function addNamesForContactId(
  contactId: string,
  newNames: NewName[],
  trx?: TransactionOrKnex
) {
  return Name.query(trx).insert(
    newNames.map((newName) => {
      return {
        ...newName,
        contactId,
      };
    })
  );
}

export async function updateNameById(
  id: string,
  nameChangeSet: NameChangeSet,
  trx?: TransactionOrKnex
) {
  return Name.query(trx).skipUndefined().update(nameChangeSet).findById(id);
}

export async function removeNameById(id: string, trx?: TransactionOrKnex) {
  return Name.query(trx).delete().findById(id);
}

export async function removeNamesByIds(ids: string, trx?: TransactionOrKnex) {
  return Name.query(trx).delete().findByIds(ids.split(","));
}

export async function setPrimaryNameForContact(
  contactId: string,
  nameId: string,
  trx?: TransactionOrKnex
) {
  const name = await Name.query(trx).findById(nameId);
  if (!name) {
    throw new NameNotFound();
  }
  if (contactId !== name?.contactId) {
    throw new NameDoesNotBelongToContact(nameId, contactId);
  }
  const contactPrimaryName = await ContactPrimaryName.query(trx).findById(
    contactId
  );
  if (!contactPrimaryName) {
    await ContactPrimaryName.query(trx).insert({
      contactId,
      primaryNameId: nameId,
    });
  } else {
    await ContactPrimaryName.query(trx)
      .update({ primaryNameId: nameId })
      .findById(contactId);
  }
}

export function getNameOrders(trx?: TransactionOrKnex) {
  return NameOrder.query(trx);
}
