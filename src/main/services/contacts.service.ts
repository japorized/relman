import { ContactPrimaryName } from "@main/models/contact-primary-name.model";
import {
  Contact,
  ContactChangeSet,
  NewContact,
} from "@main/models/contact.model";
import { Name } from "@main/models/name.model";
import { create as createName } from "./names.service";
import {
  formatPagedResult,
  PagedResult,
} from "@main/utils/format-paged-result";
import { Service } from "@main/utils/module-type-utilities";
import { raw, TransactionOrKnex } from "objection";
import Fuse from "fuse.js";

export const ContactsService: Service = {
  create,
  getAll,
  getOneById,
  getManyByIds,
  updateContactById,
  findMatchesByGivenName,
  deleteOneById,
  deleteManyById,
  searchAll,
  getKeptInViews,
  getNotKeptInViews,
  searchNotKeptInViews,
  keepInViewByIds,
  removeFromViewByIds,
  queryContactsTable,
  getUpcomingBirthdays,
};

export async function create(contact: NewContact) {
  return Contact.transaction(async (trx: TransactionOrKnex) => {
    const { primaryName, contactImage, ...remainingContactFields } = contact;
    const newContact = await Contact.query(trx).insert({
      ...remainingContactFields,
      contactImage: contactImage ? Buffer.from(contactImage) : undefined,
    });
    const newPrimaryName = await createName(
      {
        ...primaryName,
        contactId: newContact.id,
      },
      trx
    )
      .then((res: Name) => res)
      .catch((error: Error) => {
        console.error(error);
        throw error;
      });
    await ContactPrimaryName.query(trx).insert({
      contactId: newContact.id,
      primaryNameId: newPrimaryName.id,
    });
    return {
      ...newContact,
      primaryName: newPrimaryName,
    };
  });
}

export async function getAll(page = 0, limit = 50) {
  const fetched = await Contact.query()
    .withGraphFetched("[primaryName]")
    .page(page, limit);
  return formatPagedResult(fetched, page, limit);
}

export function getOneById(id: string) {
  return Contact.query()
    .withGraphFetched("[names, primaryName, relationships.primaryName]")
    .findById(id);
}

export async function getManyByIds(
  ids: string[],
  page = 0,
  limit = 50,
  trx?: TransactionOrKnex
) {
  const fetched = await Contact.query(trx)
    .withGraphFetched("[names, primaryName]")
    .whereIn("id", ids)
    .page(page, limit);
  return formatPagedResult(fetched, page, limit);
}

export async function updateContactById(
  id: string,
  contactChangeSet: ContactChangeSet,
  trx?: TransactionOrKnex
) {
  const { contactImage, ...baseContactChangeSet } = contactChangeSet;
  const contactImageAsBuffer = contactImage
    ? Buffer.from(contactImage)
    : undefined;
  await Contact.query(trx)
    .update({ contactImage: contactImageAsBuffer, ...baseContactChangeSet })
    .findById(id);
}

export async function findMatchesByGivenName(
  givenName: string,
  page = 0,
  limit = 50
) {
  const lowercasedGivenName = givenName.toLowerCase();
  return Name.transaction(async (trx) => {
    const matchingNames = await Name.query(trx)
      .distinct("contact_id")
      .whereRaw(`LOWER(names.given_name) LIKE '%${lowercasedGivenName}%'`);
    const matchingNameContactIds = matchingNames.map(
      (matchingName) => matchingName.contactId
    );
    return getManyByIds(matchingNameContactIds, page, limit, trx);
  });
}

export function deleteOneById(id: string, trx?: TransactionOrKnex) {
  return Contact.query(trx).deleteById(id);
}

export function deleteManyById(ids: string[], trx?: TransactionOrKnex) {
  return Contact.query(trx).delete().whereIn("id", ids);
}

/**
 * Search through all contacts fuzzily
 * @param searchTerm    Searched terms
 */
export async function searchAll(
  searchTerm: string,
  page = 0,
  limit = 50,
  trx?: TransactionOrKnex
): Promise<PagedResult<Contact>> {
  const allContacts = await Contact.query(trx).withGraphFetched(
    "[primaryName, names]"
  );
  const fuseIndex = new Fuse(allContacts, {
    shouldSort: true,
    keys: [
      "primaryName.givenName",
      "primaryName.surname",
      "primaryName.middleName",
      "names.givenName",
      "names.surname",
      "names.middleName",
    ],
  });

  const searchResults = fuseIndex
    .search(searchTerm)
    .map((result) => result.item);

  const offset = page * limit;
  const cutOffPoint = (page + 1) * limit;
  return <PagedResult<Contact>>{
    total: searchResults.length,
    totalPages: Math.ceil(searchResults.length / limit),
    currentPage: page,
    results: searchResults.slice(offset, cutOffPoint),
  };
}

export async function getKeptInViews(
  page = 0,
  limit = 50,
  trx?: TransactionOrKnex
) {
  const fetched = await Contact.query(trx)
    .withGraphFetched("[primaryName]")
    .where("lastContactedKeptInView", true)
    .orderBy("lastContactedAt", "ASC")
    .page(page, limit);
  return formatPagedResult(fetched, page, limit);
}

export async function getNotKeptInViews(
  page = 0,
  limit = 50,
  trx?: TransactionOrKnex
) {
  const fetched = await Contact.query(trx)
    .withGraphFetched("[primaryName]")
    .where("lastContactedKeptInView", false)
    .orderBy("lastContactedAt", "ASC")
    .page(page, limit);
  return formatPagedResult(fetched, page, limit);
}

export async function searchNotKeptInViews(
  searchTerm: string,
  trx?: TransactionOrKnex
) {
  const results = await Contact.query(trx)
    .withGraphFetched("[primaryName, names]")
    .where("lastContactedKeptInView", false)
    .orderBy("lastContactedAt", "ASC");
  const fuseIndex = new Fuse(results, {
    shouldSort: true,
    keys: [
      "primaryName.givenName",
      "primaryName.surname",
      "primaryName.middleName",
      "names.givenName",
      "names.surname",
      "names.middleName",
    ],
  });

  return fuseIndex.search(searchTerm).map((result) => result.item);
}

/**
 * @param ids   Comma-separated list of ids
 */
export function keepInViewByIds(ids: string, trx?: TransactionOrKnex) {
  return Contact.query(trx)
    .update({ lastContactedKeptInView: true })
    .findByIds(ids.split(","));
}

/**
 * @param ids   Comma-separated list of ids
 */
export async function removeFromViewByIds(
  ids: string,
  trx?: TransactionOrKnex
) {
  return Contact.query(trx)
    .update({ lastContactedKeptInView: false })
    .findByIds(ids.split(","));
}

export async function queryContactsTable(
  queryParams?: {
    relations?: string;
    where?: Partial<Omit<Contact, "relationships">>;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    whereIn?: [string, any[]][];
  },
  page = 0,
  limit = 50,
  trx?: TransactionOrKnex
) {
  let query = Contact.query(trx);

  if (queryParams) {
    if (queryParams.relations) {
      query = query.withGraphFetched(queryParams.relations);
    }

    if (queryParams.where) {
      query = query.where(queryParams.where);
    }

    if (queryParams.whereIn) {
      for (const whereIn of queryParams.whereIn) {
        query = query.whereIn(...whereIn);
      }
    }
  }

  const fetched = await query.page(page, limit);
  return formatPagedResult(fetched, page, limit);
}

export async function getUpcomingBirthdays(
  daysAway = 14,
  trx?: TransactionOrKnex
) {
  return Contact.query(trx)
    .select(
      "*",
      raw(
        `round(
          STRFTIME(
            '%J',
            (date(STRFTIME('%Y', 'now', 'localtime')||STRFTIME('-%m-%d', date_of_birth)))
          )
          - STRFTIME('%J', 'now')
        )
        as days_from_this_years_birthday`
      ),
      raw(
        `round(
          STRFTIME(
            '%J',
            (date(STRFTIME('%Y', 'now', 'localtime')||STRFTIME('-%m-%d', date_of_birth), '+1 year'))
          )
          - STRFTIME('%J', 'now')
        )
        as days_from_next_years_birthday`
      )
    )
    .withGraphFetched("[primaryName]")
    .whereRaw(
      `? >= CASE
        when days_from_this_years_birthday < 0 then days_from_next_years_birthday
        else days_from_this_years_birthday
      END`,
      [daysAway]
    )
    .orderByRaw(
      `CASE 
        when days_from_this_years_birthday < 0 then days_from_next_years_birthday
        else days_from_this_years_birthday
      END`
    );
}
