import {
  Relationship,
  SpecificRelationship,
} from "@main/models/relationship.model";
import { TransactionOrKnex } from "objection";

export const RelationshipsService = {
  create,
  remove,
};

export function create(
  newRelationships: SpecificRelationship[],
  trx?: TransactionOrKnex
) {
  return Relationship.query(trx).insert(newRelationships);
}

export function remove(
  relationship: SpecificRelationship,
  trx?: TransactionOrKnex
) {
  return Relationship.query(trx).del().where(relationship);
}
