import { Model } from "objection";

export class ContactPrimaryName extends Model {
  static tableName = "contact_primary_names";
  static idColumn = "contact_id";

  contactId: string;
  primaryNameId: string;

  static get relationMappings() {
    /* eslint-disable @typescript-eslint/no-var-requires */
    const { Contact } = require("./contact.model");
    const { Name } = require("./name.model");
    /* eslint-enable @typescript-eslint/no-var-requires */

    return {
      contact: {
        relation: Model.BelongsToOneRelation,
        modelClass: Contact,
        join: {
          from: `${ContactPrimaryName.tableName}.contact_id`,
          to: `${Contact.tableName}.id`,
        },
      },
      primaryName: {
        relation: Model.BelongsToOneRelation,
        modelClass: Name,
        join: {
          from: `${ContactPrimaryName}.primary_name_id`,
          to: `${Name.tableName}.id`,
        },
      },
    };
  }
}
