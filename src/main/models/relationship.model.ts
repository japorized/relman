import { Model } from "objection";

export class Relationship extends Model {
  static tableName = "relationships";

  static get idColumn() {
    return [
      "contact1_id",
      "contact2_id",
      "contact1_role_id",
      "contact2_role_id",
    ];
  }

  contact1Id: string;
  contact2Id: string;
  contact1RoleId: string;
  contact2RoleId: string;

  static get relationMappings() {
    /* eslint-disable @typescript-eslint/no-var-requires */
    const { Contact } = require("./contact.model");
    const { Role } = require("./role.model");
    /* eslint-enable @typescript-eslint/no-var-requires */

    return {
      contact1: {
        relation: Model.BelongsToOneRelation,
        modelClass: Contact,
        join: {
          from: `${Relationship.tableName}.contact1_id`,
          to: `${Contact.tableName}.id`,
        },
      },
      contact2: {
        relation: Model.BelongsToOneRelation,
        modelClass: Contact,
        join: {
          from: `${Relationship.tableName}.contact2_id`,
          to: `${Contact.tableName}.id`,
        },
      },
      contact1Role: {
        relation: Model.HasOneRelation,
        modelClass: Role,
        join: {
          from: `${Relationship.tableName}.contact1_role_id`,
          to: `${Role.tableName}.id`,
        },
      },
      contact2Role: {
        relation: Model.HasOneRelation,
        modelClass: Role,
        join: {
          from: `${Relationship.tableName}.contact2_role_id`,
          to: `${Role.tableName}.id`,
        },
      },
    };
  }
}

export type SpecificRelationship = Pick<
  Relationship,
  "contact1Id" | "contact2Id" | "contact1RoleId" | "contact2RoleId"
>;
