import { Model } from "objection";
import { $dayjs } from "../utils/dayjs";
import { Name, NewName } from "./name.model";

export type ContactRelationship = Contact & {
  contactRole: string;
  relationshipRole: string;
};

export class Contact extends Model {
  static tableName = "contacts";

  id: string;
  dateOfBirth?: string;
  contactImage?: Buffer;
  createdAt: Date;
  updatedAt: Date;
  lastContactedAt?: Date;
  lastContactedKeptInView: boolean;

  names?: Name[];
  primaryName?: Name;
  relationships?: Array<ContactRelationship>;

  static get relationMappings() {
    /* eslint-disable @typescript-eslint/no-var-requires */
    const { Name } = require("./name.model");
    const { ContactPrimaryName } = require("./contact-primary-name.model");
    const { Relationship } = require("./relationship.model");
    /* eslint-enable @typescript-eslint/no-var-requires */

    return {
      names: {
        relation: Model.HasManyRelation,
        modelClass: Name,
        join: {
          from: `${Contact.tableName}.id`,
          to: `${Name.tableName}.contactId`,
        },
      },
      primaryName: {
        relation: Model.HasOneThroughRelation,
        modelClass: Name,
        join: {
          from: `${Contact.tableName}.id`,
          through: {
            from: `${ContactPrimaryName.tableName}.contactId`,
            to: `${ContactPrimaryName.tableName}.primaryNameId`,
          },
          to: `${Name.tableName}.id`,
        },
      },
      relationships: {
        relation: Model.ManyToManyRelation,
        modelClass: Contact,
        join: {
          from: `${Contact.tableName}.id`,
          through: {
            from: `${Relationship.tableName}.contact1Id`,
            extra: {
              contactRole: "contact1RoleId",
              relationshipRole: "contact2RoleId",
            },
            to: `${Relationship.tableName}.contact2Id`,
          },
          to: `${Contact.tableName}.id`,
        },
      },
    };
  }

  $afterGet() {
    this.createdAt = new Date(this.createdAt);
    this.updatedAt = new Date(this.updatedAt);
    if (this.lastContactedAt)
      this.lastContactedAt = new Date(this.lastContactedAt);
    if (this.lastContactedKeptInView !== null) {
      this.lastContactedKeptInView =
        (this.lastContactedKeptInView as unknown as number) === 0
          ? false
          : true;
    }
  }

  $beforeInsert() {
    this.createdAt = new Date();
    this.updatedAt = new Date();
    this.transformInput();
  }

  $beforeUpdate() {
    this.updatedAt = new Date();
    this.transformInput();
  }

  private transformInput() {
    if (this.dateOfBirth) {
      this.dateOfBirth = $dayjs(this.dateOfBirth).local().format("YYYY-MM-DD");
    }
    if (this.lastContactedAt)
      this.lastContactedAt = new Date(this.lastContactedAt);
  }
}

export type NewContact = Pick<Contact, "dateOfBirth"> & {
  contactImage?: BinaryType;
  primaryName: NewName;
};

export type ContactChangeSet = Partial<Omit<NewContact, "primaryName">> &
  Partial<Pick<Contact, "lastContactedAt" | "lastContactedKeptInView">>;
