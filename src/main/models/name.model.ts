import { Model } from "objection";

export enum NameOrderType {
  Irrelevant = "irrelevant",
  FirstMiddleLast = "first_middle_last",
  FamilyMiddleGiven = "family_middle_given",
}

export class NameOrder extends Model {
  static tableName = "name_orders";

  id: string;
  type: NameOrderType;
}

export class Name extends Model {
  static tableName = "names";

  id: string;
  contactId: string;
  givenName: string;
  surname?: string;
  middleName?: string;
  title?: string;
  nameOrderId: string;

  static get relationMappings() {
    /* eslint-disable @typescript-eslint/no-var-requires */
    const Contact = require("./contact.model");
    /* eslint-enable @typescript-eslint/no-var-requires */

    return {
      contact: {
        relation: Model.HasOneRelation,
        modelClass: Contact,
        join: {
          from: `${Name.tableName}.contact_id`,
          to: `${Contact.tableName}.id`,
        },
      },
      nameOrder: {
        relation: Model.HasOneRelation,
        modelClass: NameOrder,
        join: {
          from: `${Name.tableName}.name_order_id`,
          to: `${NameOrder.tableName}.id`,
        },
      },
    };
  }
}

export type NewName = Pick<
  Name,
  "contactId" | "givenName" | "surname" | "middleName" | "title" | "nameOrderId"
>;

export type NameChangeSet = Partial<NewName> & Partial<Pick<Name, "id">>;
