import { BrowserWindow, protocol } from "electron";
import {
  default as installExtension,
  VUEJS3_DEVTOOLS,
} from "electron-devtools-installer";
import * as path from "path";
import { initializeDbConnection } from "./ipc-modules/database";
import { $app } from "./app-setup";
import { MainModule } from "./ipc-modules/index";
import { Configurations } from "./lib/configurations";
import { ProtocolScheme } from "./lib/protocol-schemes";

function createWindow() {
  const win = new BrowserWindow({
    autoHideMenuBar: true,
    webPreferences: {
      // We're using unsafe defaults here, consciously, as we can't import
      // ipcRenderer normally on the renderer
      // (no strict Node.js dependency for Vite)
      contextIsolation: false,
      nodeIntegration: true,
      preload: path.join(process.cwd(), "dist/preload/index.js"),
      sandbox: true,
    },
    show: false,
    title: $app.getName(),
  });

  win.once("ready-to-show", () => {
    win.show();
  });

  win.loadURL(
    process.env["NODE_ENV"] === "production"
      ? `file://${__dirname}/../renderer/index.html`
      : process.env.VITE_DEV_SERVER_URL || "http://localhost:3000"
  );
}

$app.whenReady().then(async () => {
  await installExtension([VUEJS3_DEVTOOLS])
    .then((name: string) => {
      console.log(`Added Extension: ${name}`);
    })
    .catch((err: Error) => {
      console.error("An error occurred: ", err);
      process.exit(1);
    });
  createWindow();
  const configs = new Configurations($app);
  initializeDbConnection();

  new MainModule(configs);
});

$app.on("window-all-closed", () => {
  protocol.unregisterProtocol(ProtocolScheme.UserData);
  if (process.platform !== "darwin") {
    $app.quit();
  }
});

$app.on("activate", () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
