/**
 * @function convertEnvVar
 *
 * @param   str   String to have its substrings that are environment variables converted
 *
 * @return String with its environment variables realized
 */
export function convertEnvVar(str: string): string {
  const envVarRegex = /\$\w+/g;

  // Big bad hax to deep copy a string cause
  // 1. Typescript treats String and string differently
  // 2. V8 [Bug #2869](https://code.google.com/p/v8/issues/detail?id=2869)
  let strCopy = (" " + str).slice(1);

  let match: Array<string> | null;
  while ((match = envVarRegex.exec(str)) !== null) {
    // TODO: This will only work for POSIX
    // We'll need a better thing here to support Windows
    const envKey = process.env[match[0].replace(/^\$/, "")];
    if (envKey) strCopy = strCopy.replace(match[0], envKey);
  }
  return strCopy;
}
