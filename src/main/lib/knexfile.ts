import { knexSnakeCaseMappers } from "objection";
import { Configurations } from "./configurations";
import { join } from "path";
import { $app } from "@main/app-setup";

const configs = new Configurations($app);

export default {
  development: {
    client: "sqlite3",
    useNullAsDefault: true,
    connection: {
      filename: configs.databaseFile.fullpath,
    },
    migrations: {
      extension: "ts",
      directory: join(__dirname, "../../database/migrations"),
      tableName: "knex_migrations",
    },
    seeds: {
      directory: join(__dirname, "../../database/development-seeds"),
    },
    ...(process.env.IS_MIGRATION === "true" ? {} : knexSnakeCaseMappers()),
  },
  production: {
    client: "sqlite3",
    useNullAsDefault: true,
    connection: {
      filename: configs.databaseFile.fullpath,
    },
    migrations: {
      extension: "ts",
      directory: join(__dirname, "../../database/migrations"),
      tableName: "knex_migrations",
    },
    seeds: {
      directory: join(__dirname, "../../database/production-seeds"),
    },
    ...knexSnakeCaseMappers(),
    ...(process.env.IS_MIGRATION === "true" ? {} : knexSnakeCaseMappers()),
  },
};
