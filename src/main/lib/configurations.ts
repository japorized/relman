import { App } from "electron";
import { resolve as resolvePath } from "path";
import { FilePath } from "./file-path";
import * as fs from "fs";
import * as sqlite3 from "sqlite3";
import { DirPath } from "./dir-path";

type ConfigurationFileContents = {
  colorscheme: BuiltInColorscheme | string;
  motion: Motion;
  databaseFile: string;
  dataDir: string;
};

export enum Motion {
  Default = "default",
  Reduced = "reduced",
}

export enum BuiltInColorscheme {
  Light = "light",
  Dark = "dark",
  System = "system",
}

export class Configurations {
  private configFile: FilePath;
  colorscheme: BuiltInColorscheme | string;
  motion: Motion;
  databaseFile: FilePath;
  dataDir: DirPath;
  cacheDir: DirPath;

  constructor(app: App) {
    const configDir = resolvePath(app.getPath("appData"), app.getName());
    this.configFile = new FilePath(resolvePath(configDir, "config.json"));

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const userConfiguration: any = {};
    try {
      const file = fs.readFileSync(this.configFile.fullpath, {
        encoding: "utf-8",
      });
      Object.assign(userConfiguration, JSON.parse(file));
    } catch (error) {
      console.error(error);
      console.info("Could not load config file. Will use default configs.");
    }

    this.colorscheme =
      userConfiguration.colorscheme ?? BuiltInColorscheme.System;
    this.motion = userConfiguration.motion ?? Motion.Default;
    this.databaseFile = userConfiguration.databaseFile
      ? new FilePath(userConfiguration.databaseFile)
      : new FilePath(resolvePath(configDir, "default.db"));
    this.dataDir = userConfiguration.dataDir
      ? new DirPath(userConfiguration.dataDir)
      : new DirPath(app.getPath("userData"));

    this.cacheDir = new DirPath(app.getPath("cache"));

    this.createDbFileIfNotExist();
  }

  /**
   * @function save
   *
   * Saves the user's configurations to the location specified from
   * getConfigFilePath.
   *
   * @returns A promise of whether the saving process was a success
   */
  async save() {
    const configs: ConfigurationFileContents = {
      colorscheme: this.colorscheme,
      motion: this.motion,
      databaseFile: this.databaseFile.toString(),
      dataDir: this.dataDir.toString(),
    };
    // Create the appropriate directory for the config file
    if (!fs.existsSync(this.configFile.dir)) {
      fs.mkdirSync(this.configFile.dir);
    }
    // Try writing to the config file
    try {
      fs.writeFileSync(
        this.configFile.fullpath,
        JSON.stringify(configs, null, 2)
      );
      return true;
    } catch (error) {
      console.error(error);
      return false;
    }
  }

  async createDbFileIfNotExist() {
    if (this.databaseFile) {
      if (!fs.existsSync(this.databaseFile.fullpath)) {
        if (!fs.existsSync(this.databaseFile.dir)) {
          fs.mkdirSync(this.databaseFile.dir, { recursive: true });
        }
        // Create the file
        new sqlite3.Database(this.databaseFile.fullpath);
        return true;
      }
    }
    return false;
  }

  toString() {
    return `${{ ...this }}`;
  }
}
