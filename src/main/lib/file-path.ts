import { convertEnvVar } from "./convert-env-var";
import * as path from "path";
import * as fs from "fs";

export class FilePath {
  rawDir: string;
  dir: string;
  rawFilename: string;
  filename: string;
  readonly rawFullPath: string;
  readonly fullpath: string;

  constructor(givenPath: string) {
    this.rawDir = path.posix.dirname(givenPath).normalize();
    this.rawFilename = path.posix.basename(givenPath).normalize();
    this.dir = convertEnvVar(this.rawDir);
    this.filename = convertEnvVar(this.rawFilename);
    this.rawFullPath = givenPath;
    this.fullpath = convertEnvVar(givenPath);

    if (!fs.existsSync(this.dir)) {
      fs.mkdirSync(this.dir, { recursive: true });
    }
  }

  toString() {
    return this.rawFullPath;
  }

  toStaticString() {
    return this.fullpath;
  }
}
