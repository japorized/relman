import snake from "to-snake-case";

export class BaseError extends Error {
  code: string;
  timestamp: string;

  constructor(msg: string) {
    super(msg);

    this.code = snake(this.name);
    this.timestamp = new Date().toISOString();
  }
}

export class NameNotFound extends BaseError {
  constructor(msg?: string) {
    super(msg || "Name not found");
  }
}

export class NameDoesNotBelongToContact extends BaseError {
  constructor(nameId: string, contactId: string) {
    super(`Name ${nameId} does not belong to contact ${contactId}`);
  }
}
