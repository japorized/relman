import snake from "to-snake-case";
import { ipcMain } from "electron";
import { Service } from "@main/utils/module-type-utilities";

export class BaseIpcModule {
  constructor(handlerPrefix: string, services: Service[]) {
    console.log(`Initializing ${handlerPrefix}`);
    for (const service of services) {
      for (const fn in service) {
        const handlerName = `${handlerPrefix}:${snake(fn)}`;
        console.log(`Registering handler: ${handlerName}`);
        ipcMain.handle(handlerName, (_event, ...args: unknown[]) =>
          service[fn](...args)
        );
      }
    }
  }
}
