import { BaseIpcModule } from "./base-ipc.module";
import { RelationshipsService } from "@main/services/relationships.service";

export class RelationshipsModule extends BaseIpcModule {
  constructor() {
    super("relationships", [RelationshipsService]);
  }
}
