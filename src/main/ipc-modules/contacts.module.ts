import { ContactsService } from "@main/services/contacts.service";
import { BaseIpcModule } from "./base-ipc.module";

export class ContactsModule extends BaseIpcModule {
  constructor() {
    super("contacts", [ContactsService]);
  }
}
