import { Model } from "objection";
import knex from "knex";
import knexfile from "../lib/knexfile";

const env = process.env["NODE_ENV"] ?? "development";

export function initializeDbConnection() {
  const knexInstance = knex(
    env === "development" ? knexfile.development : knexfile.production
  );

  Model.knex(knexInstance);
}
