import { Configurations } from "@main/lib/configurations";
import { ProtocolScheme } from "@main/lib/protocol-schemes";
import { protocol } from "electron";
import * as path from "path";
import { ConfigurationsModule } from "./configurations";
import { ContactsModule } from "./contacts.module";
import { NamesModule } from "./names.module";
import { OsModule } from "./os.module";
import { RelationshipsModule } from "./relationships.module";
import { RolesModule } from "./roles.module";

export class MainModule {
  constructor(configs: Configurations) {
    new ConfigurationsModule(configs);

    console.log("Initializing IPC modules");
    new ContactsModule();
    new NamesModule();
    new RolesModule();
    new RelationshipsModule();

    new OsModule();

    console.log("Setup utility file protocols");
    protocol.registerFileProtocol(
      ProtocolScheme.UserData,
      (request, callback) => {
        const url = request.url.replace(`${ProtocolScheme.UserData}://`, "");
        callback({
          path: path.normalize(`/${configs.dataDir.fullpath}/${url}`),
        });
      }
    );
  }
}
