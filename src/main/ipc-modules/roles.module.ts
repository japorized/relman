import { BaseIpcModule } from "./base-ipc.module";
import { RolesService } from "@main/services/roles.service";

export class RolesModule extends BaseIpcModule {
  constructor() {
    super("roles", [RolesService]);
  }
}
