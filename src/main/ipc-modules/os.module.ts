import { BaseIpcModule } from "./base-ipc.module";
import { OsService } from "@main/services/os.service";

export class OsModule extends BaseIpcModule {
  constructor() {
    super("os", [OsService]);
  }
}
