import { NamesService } from "@main/services/names.service";
import { BaseIpcModule } from "./base-ipc.module";

export class NamesModule extends BaseIpcModule {
  constructor() {
    super("names", [NamesService]);
  }
}
