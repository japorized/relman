import { FilePath } from "@main/lib/file-path";
import { Configurations } from "@main/lib/configurations";
import { ipcMain } from "electron";

export class ConfigurationsModule {
  constructor(configs: Configurations) {
    ipcMain.on("configs:get", (event) => {
      event.returnValue = { ...configs };
    });

    ipcMain.handle(
      "configs:save",
      async (_event, newConfig: Partial<Configurations>) => {
        const configKeys = Reflect.ownKeys(configs) as string[];
        for (const key of configKeys) {
          const newConfigValue = Reflect.get(newConfig, key) as
            | string
            | number
            | undefined;
          if (newConfigValue !== undefined) {
            if (["databaseFile", "dataDir"].includes(key)) {
              Reflect.set(configs, key, new FilePath(newConfigValue as string));
            } else {
              Reflect.set(configs, key, newConfigValue);
            }
          }
        }
        console.debug("Saving new configurations...");
        return configs.save();
      }
    );
  }
}
