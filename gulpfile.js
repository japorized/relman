/* eslint-disable @typescript-eslint/no-var-requires */
const gulp = require("gulp");
const path = require("path");
const { spawn } = require("child_process");
const electron = require("electron");
const { createServer, build: viteBuild } = require("vite");
const { log, COLOR } = require("./.scripts/log");
const ts = require("gulp-typescript");
const tsAlias = require("gulp-ts-alias");

const baseTsConfig = require("./tsconfig.json");
/* eslint-enable @typescript-eslint/no-var-requires */

const DIST_DIR = path.resolve(__dirname, "dist");
const GLOB_MAIN_SRC = "src/main/**/*";
const GLOB_MAIN_DIST = "dist/main/**/*";
const GLOB_PRELOAD_SRC = "src/preload/**/*";
const GLOB_PRELOAD_DIST = "dist/preload/**/*";
const GLOB_DATABASE_SRC = "src/database/**/*";

const env = process.env.NODE_ENV ?? "development";

let electronProcess, viteDevProcess;

function setupLog(cprocess, name, color) {
  if (!Object.values(COLOR).includes(color) && !color) {
    console.error("Invalid color selected.");
    console.info("Acceptable colors include:", COLOR);
    throw "Unaccepted color passed to setupLog";
  }

  const logger = (buffer) => {
    buffer
      .toString()
      .split(/\n/)
      .filter((message) => message !== "")
      .forEach((message) => log(color)(name, message));
  };

  cprocess.stdout.on("data", logger);
  cprocess.stderr.on("data", logger);
}

function compileMain(cb) {
  const mainTsProject = ts.createProject("./src/main/tsconfig.json");
  try {
    gulp
      .src("./src/main/**/*.ts")
      .pipe(tsAlias({ config: baseTsConfig.compilerOptions }))
      .pipe(mainTsProject())
      .js.pipe(gulp.dest("./dist/main"));
  } catch (error) {
    console.error(error);
  }
  cb();
}

function compilePreload(cb) {
  const preloadTsProject = ts.createProject("./src/preload/tsconfig.json");
  try {
    gulp
      .src("./src/preload/**/*.ts")
      .pipe(tsAlias({ config: baseTsConfig.compilerOptions }))
      .pipe(preloadTsProject())
      .js.pipe(gulp.dest("./dist/preload"));
  } catch (error) {
    console.error(error);
  }
  cb();
}

function compileDatabaseFiles(cb) {
  const databaseTsProject = ts.createProject("./src/database/tsconfig.json");
  try {
    gulp
      .src("./src/database/**/*.ts")
      .pipe(tsAlias({ config: baseTsConfig.compilerOptions }))
      .pipe(databaseTsProject())
      .js.pipe(gulp.dest("./dist/database"));
  } catch (error) {
    console.error(error);
  }
  cb();
}

const compile = gulp.parallel(
  compileMain,
  compilePreload,
  compileDatabaseFiles
);

async function startVite(cb) {
  viteDevProcess = await createServer({
    mode: env,
    clearScreen: false,
    configFile: path.join(process.cwd(), "vite.config.js"),
  });

  // Reload page on preload script change
  // viteDevProcess.watcher.add(join(process.cwd(), "src/renderer/**/*"));
  // viteDevProcess.watcher.on("change", (file) => {
  //   file = normalizePath(file);

  //   viteDevProcess.ws.send({
  //     type: "full-reload",
  //     path:
  //       "/" + normalizePath(path.relative(viteDevProcess.config.root, file)),
  //   });
  // });

  // Run Vite server
  await viteDevProcess.listen();

  // Determining the current URL of the server. It depend on
  // /config/renderer.vite.js Write a value to an environment variable to pass
  // it to the main process.
  {
    const protocol = `http${viteDevProcess.config.server.https ? "s" : ""}:`;
    const host = viteDevProcess.config.server.host || "localhost";
    const port = viteDevProcess.config.server.port; // Vite searches for and occupies the first free
    // port: 3000, 3001, 3002 and so on
    const path = "/";
    process.env.VITE_DEV_SERVER_URL = `${protocol}//${host}:${port}${path}`;
  }
  cb();
}

function startElectron(cb) {
  electronProcess = spawn(electron, [
    "--inspect=9229",
    `${DIST_DIR}/main/index.js`,
  ]);
  setupLog(electronProcess, "electron", COLOR.Magenta);
  cb();
}

function stopElectron(cb) {
  if (electronProcess) electronProcess.kill();
  cb();
}

function watch(cb) {
  gulp.watch(GLOB_MAIN_SRC, compileMain);
  gulp.watch(GLOB_PRELOAD_SRC, compilePreload);
  gulp.watch(GLOB_DATABASE_SRC, compileDatabaseFiles);
  gulp.watch(GLOB_MAIN_DIST, gulp.series(stopElectron, startElectron));
  gulp.watch(GLOB_PRELOAD_DIST, gulp.series(stopElectron, startElectron));
  cb();
}

async function buildProduction(cb) {
  compile();
  await viteBuild({
    base: "./",
    build: { outDir: path.resolve(DIST_DIR, "renderer") },
  });
  cb();
}

exports.compile_main = compileMain;
exports.compile_preload = compilePreload;
exports.compile_database_files = compileDatabaseFiles;
exports.compile = compile;
exports.build_production = buildProduction;
exports.start_electron = startElectron;
exports.watch = watch;
exports.default = gulp.series(compile, startVite, watch);
