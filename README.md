# relman

An offline-first personal relationship manager

## Status: Pre-alpha

---

## Development Setup

```
# Git setup (with suggestions)
git clone https://gitlab.com/japorized/relman
cd relman
git remote rename origin upstream
git remote add origin git@gitlab.com:<your_username>/relman.git

# Setup project
yarn setup

# Develop
yarn dev
```

- Yet to figure out how packaging the project would work.

---

## Project Organization

Relevant source files are currently split into 3 directories:
```
relman
└── src
    ├── main          # The main running Electron process
    ├── preload       # Preload scripts to be ran at the beginning of the Electron process
    └── renderer      # Frontend rendering sources
```

--- 

## Current known dev issues

- The dev script needs a lot of work —
  - logs from Vite is just flat out ugly and makes logs rather awkward to use.
    - Unfortunately, this needs Vite to improve its configurability of logs
