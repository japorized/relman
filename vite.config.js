import vue from "@vitejs/plugin-vue";
import vueJsx from "@vitejs/plugin-vue-jsx";
import path from "path";
import { defineConfig } from "vite";
import Pages from "vite-plugin-pages";

const env = process.env.NODE_ENV ?? "development";

// https://vitejs.dev/config/
export default defineConfig({
  server: {
    host: "localhost",
    port: process.env.PORT ?? "3000",
    watch: path.resolve(__dirname, "./src/renderer") + "**/*",
  },
  logLevel: env === "development" ? "info" : "warn",
  resolve: {
    alias: [
      { find: "@", replacement: path.resolve(__dirname, "./src") },
      { find: "@main", replacement: path.resolve(__dirname, "./src/main") },
      {
        find: "@renderer",
        replacement: path.resolve(__dirname, "./src/renderer"),
      },
    ],
  },
  plugins: [
    vue(),
    vueJsx({
      optimize: true,
    }),
    Pages({
      pagesDir: "src/renderer/pages",
    }),
  ],
  clearScreen: false,
});
